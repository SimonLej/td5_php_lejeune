<?php

return [
    // Database
    'database' => [
        'engine' => 'mysql',
        'host' => 'localhost',
        'database' => 'library',
        'user' => 'library',
        'password' => 'library'
    ],
    // Administrator auth
    'admin'  => ['admin', 'password'],
    'admin1' => ['admin', 'password1'],
    'admin2' => ['admin', 'password2']
];
