<?php

use Gregwar\Image\Image;

$app->match('/', function() use ($app) {
    return $app['twig']->render('home.html.twig');
})->bind('home');

$app->match('/books', function() use ($app) {
    return $app['twig']->render('books.html.twig', array(
        'books' => $app['model']->getBooks()
    ));
})->bind('books');

$app->match('/books/{id}', function($id) use ($app){
    return $app['twig']->render('book.html.twig', array(
        'book' => $app['model']->getBook($id),
        'NbExemplaires' => $app['model']->getNbExemplaires($id),
        'exemplaires' => $app['model']->getExemplaires($id),
        'emprunts' => $app['model']->getEmprunte($id)
    ));
})->bind('book');

$app->match('/emprunt/{id}', function($id) use ($app){
    if (!$app['session']->has('admin')) {
        return $app['twig']->render('shouldBeAdmin.html.twig');
    }
    $request = $app['request'];
    $success = false;
    if ($request->getMethod() == 'POST') {
        $post = $request->request;
        if ($post->has('nom') && $post->has('dateFin')) {
            $dateFin = $post->get('dateFin');
            $dateFin = new DateTime($dateFin);
            $day = $dateFin->format('d');
            $mois = $dateFin->format('m');
            $year = $dateFin->format('y');
            if ($day > date('d') && $mois >= date('m') && $year >= date('y')) {
                $app['model']->setEmprunt($post->get('nom'), $post->get('dateFin'), $id);
                $success = true;
            }
        }
    }
    return $app['twig']->render('emprunt.html.twig', array(
        'success' => $success
    ));
})->bind('emprunt');

$app->match('/retour/{idBook}/{id}', function($idBook,$id) use ($app){
    $app['model']->setRetour($id);
    return $app->redirect($app['url_generator']->generate('book', array('id' => $idBook)));
})->bind('retour');

$app->match('/admin', function() use ($app) {
    $request = $app['request'];
    $success = false;
    if ($request->getMethod() == 'POST') {
        $post = $request->request;
        if ($post->has('login') && $post->has('password') &&
            array('admin', $post->get('password')) == $app['config'][$post->get('login')]) {
            $app['session']->set('admin', true);
            $success = true;
        }
    }
    return $app['twig']->render('admin.html.twig', array(
        'success' => $success
    ));
})->bind('admin');

$app->match('/logout', function() use ($app) {
    $app['session']->remove('admin');
    return $app->redirect($app['url_generator']->generate('admin'));
})->bind('logout');

$app->match('/addBook', function() use ($app) {
    if (!$app['session']->has('admin')) {
        return $app['twig']->render('shouldBeAdmin.html.twig');
    }
    $success = false;
    $request = $app['request'];
    if ($request->getMethod() == 'POST') {
        $post = $request->request;
        if ($post->has('title') && $post->has('author') && $post->has('synopsis') &&
            $post->has('copies')) {
            $files = $request->files;
            $image = '';

            // Resizing image
            if ($files->has('image') && $files->get('image')) {
                $image = sha1(mt_rand().time());
                Image::open($files->get('image')->getPathName())
                    ->resize(240, 300)
                    ->save('uploads/'.$image.'.jpg');
                Image::open($files->get('image')->getPathName())
                    ->resize(120, 150)
                    ->save('uploads/'.$image.'_small.jpg');
            }

            // Saving the book to database
            $app['model']->insertBook($post->get('title'), $post->get('author'), $post->get('synopsis'),
                $image, (int)$post->get('copies'));
            $success = true;
        }
    }

    return $app['twig']->render('addBook.html.twig', array(
        'success' => $success));
})->bind('addBook');

